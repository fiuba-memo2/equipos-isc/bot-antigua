class JobvacancyConnectionError < StandardError
end

class ApiConnector
  def initialize
    @conn = Faraday::Connection.new jobvacancy_url
  end

  def offers_service
    offers_url = "#{jobvacancy_url}/api/offers"
    response = @conn.get offers_url
    loop do
      response = @conn.get offers_url
      break unless response.body == '<h1>Internal Server Error</h1>'
    end
    raise Faraday::Error::ConnectionFailed unless response.success?

    JSON.parse(response.body)
  end

  def offers_on_date_service(date)
    conn = Faraday::Connection.new jobvacancy_url
    api_url = "/api/offers_on?date=#{date}"
    response = conn.get api_url
    loop do
      response = conn.get api_url
      break unless response.body == '<h1>Internal Server Error</h1>'
    end
    raise Faraday::Error::ConnectionFailed unless response.success?

    JSON.parse(response.body)
  end

  def register_service(name, email, password, telegram_username)
    response = '<h1>Internal Server Error</h1>'
    while response == '<h1>Internal Server Error</h1>'
      response = Faraday.post(
        "#{jobvacancy_url}/api/register",
        { user: {
          name:,
          email:,
          password:,
          telegram_username:
        } }
      ).body
    end
    JSON.parse(response)
  end

  def my_offers_service(telegram_username)
    conn = Faraday::Connection.new jobvacancy_url
    api_url = "/api/my_offers?telegram_username=#{telegram_username}"
    response = conn.get api_url
    loop do
      response = conn.get api_url
      break unless response.body == '<h1>Internal Server Error</h1>'
    end
    raise JobvacancyConnectionError unless response.success?

    response.body
  end
end
