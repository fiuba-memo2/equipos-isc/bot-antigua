require 'json'

def parse_offers(job_offers)
  job_offers_list = job_offers['offers']

  result = ''
  result += "Job Offers:\n"
  result += "-----------\n"

  job_offers_list.each do |offer|
    result += "Offer ID: #{offer['id']}\n"
    result += "Title: #{offer['title']}\n"
    result += "Location: #{offer['location']}\n"
    result += "Description: #{offer['description']}\n"
    result += "\n"
  end
  result
end

def parse_register(response)
  if response['success']
    ['User created successfully']
  else
    texts = []
    response['errors'].each do |key, value|
      if value.include?("can't be blank")
        texts.append "The argument <#{key}> cannot be empty!"
      else
        texts.append "Please enter a valid #{key}"
      end
    end
    texts
  end
end

def parse_apply(response)
  response = 'Application sent!' if response == 'Contact information sent.'
  response = 'The offer you selected does not exist!' if response == 'That offer does not exist.'
  response = 'The email you entered is not valid!' if response == 'Invalid email or empty CV url.'
  response = 'The cv url you entered is not valid!' if response == 'Invalid CV url.'
  response
end

def print_commands_for(name)
  commands = { "/start": 'Returns Hi, {your name}',
               "/offers": 'See available job offers',
               "/today_offers": 'See available job offers created today',
               "/my_offers": 'See the offers you have created',
               "/apply <offer_id>, <email>, <url cv>": 'Apply to a specific job offer',
               "/register <name>, <email>, <password>": 'Register as a job offerer',
               "/version": 'See current version of this bot',
               "/stop": 'Returns Bye, {your telegram username}',
               "/help": 'See list of available commands' }

  response = "Hi, #{name}! These are the available commands:\n\n"
  commands.each do |key, value|
    response += "#{key}\n#{value}\n\n"
  end
  response
end

def jobvacancy_url
  ENV['JOBVACANCY_API_URL']
end
