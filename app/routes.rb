require "#{File.dirname(__FILE__)}/../lib/routing"
require "#{File.dirname(__FILE__)}/../lib/version"
require "#{File.dirname(__FILE__)}/tv/series"
require "#{File.dirname(__FILE__)}/helpers/api"
require "#{File.dirname(__FILE__)}/api_connector"
require 'json'

class Routes
  include Routing

  on_message '/start' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: "Hi, #{message.from.first_name}")
  end

  on_message '/stop' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: "Bye, #{message.from.username}")
  end

  on_message '/version' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: Version.current)
  end

  on_message '/offers' do |bot, message|
    job_offers = ApiConnector.new.offers_service
    response = if job_offers['offers'].empty?
                 'There are no job offers available at the moment!'
               else
                 parse_offers(job_offers)
               end

    bot.api.send_message(chat_id: message.chat.id, text: response)
  end

  on_message_pattern %r{/register(?<params>.*)} do |bot, message, args|
    params = args['params'] << ' '
    params = params.split(',').map(&:strip)
    if params.length != 3
      texts = ['Usage: /register <name>, <email>, <password>']
    else
      response = ApiConnector.new.register_service(params[0], params[1], params[2], message.from.username)
      texts = parse_register response
    end
    texts.each do |text|
      bot.api.send_message(chat_id: message.chat.id, text:)
    end
  end

  on_message_pattern %r{/apply (?<offer_id>.*), (?<email>.*), (?<url_cv>.*)} do |bot, message, args|
    response = '<h1>Internal Server Error</h1>'
    while response == '<h1>Internal Server Error</h1>'
      response = Faraday.post("#{jobvacancy_url}/api/apply", {
                                offer_id: args['offer_id'],
                                applicant_email: args['email'],
                                url_cv: args['url_cv']
                              }).body
    end

    response = parse_apply response

    bot.api.send_message(chat_id: message.chat.id, text: response)
  end

  on_message '/help' do |bot, message|
    response = print_commands_for(message.from.first_name)

    bot.api.send_message(chat_id: message.chat.id, text: response)
  end

  on_message '/today_offers' do |bot, message|
    connector = ApiConnector.new
    begin
      job_offers = connector.offers_on_date_service(Time.at(message.date).to_date)
      response =
        if job_offers['offers'].empty?
          "I'm sorry, it seems that there are no offers posted today!"
        else
          parse_offers(job_offers)
        end
    rescue JobvacancyConnectionError
      response = 'Jobvacancy service is down...'
    end

    bot.api.send_message(chat_id: message.chat.id, text: response)
  end

  on_message '/my_offers' do |bot, message|
    connector = ApiConnector.new
    my_offers = connector.my_offers_service(message.from.username)

    response =
      if my_offers == 'User not found'
        "I'm sorry, I could not find a user linked with your telegram username"
      else
        my_offers_json = JSON.parse(my_offers)
        my_offers_json['offers'].empty? ? 'You have no offers created' : parse_offers(my_offers_json)
      end

    bot.api.send_message(chat_id: message.chat.id, text: response)
  end

  default do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: 'Oops, I do not understand! Can you repeat your question?')
  end
end
