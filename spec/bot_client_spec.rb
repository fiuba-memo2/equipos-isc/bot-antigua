require 'spec_helper'
require 'web_mock'
# Uncomment to use VCR
# require 'vcr_helper'

require "#{File.dirname(__FILE__)}/../app/bot_client"

def when_i_send_text(token, message_text, timedate=1_557_782_998)
  body = { "ok": true, "result": [{ "update_id": 693_981_718,
                                    "message": { "message_id": 11,
                                                 "from": { "id": 141_733_544, "is_bot": false, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "language_code": 'en' },
                                                 "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                                                 "date": timedate, "text": message_text,
                                                 "entities": [{ "offset": 0, "length": 6, "type": 'bot_command' }] } }] }

  stub_request(:any, "https://api.telegram.org/bot#{token}/getUpdates")
    .to_return(body: body.to_json, status: 200, headers: { 'Content-Length' => 3 })
end

def then_i_get_text(token, message_text)
  body = { "ok": true,
           "result": { "message_id": 12,
                       "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                       "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                       "date": 1_557_782_999, "text": message_text } }

  stub_request(:post, "https://api.telegram.org/bot#{token}/sendMessage")
    .with(
      body: { 'chat_id' => '141733544', 'text' => message_text }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

describe 'BotClient' do
  it 'should get a /version message and respond with current version' do
    token = 'fake_token'

    when_i_send_text(token, '/version')
    then_i_get_text(token, Version.current)

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a /start message and respond with Hola' do
    token = 'fake_token'

    when_i_send_text(token, '/start')
    then_i_get_text(token, 'Hi, Emilio')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a /stop message and respond with Chau' do
    token = 'fake_token'

    when_i_send_text(token, '/stop')
    then_i_get_text(token, 'Bye, egutter')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get an unknown message message and respond with Do not understand' do
    token = 'fake_token'

    when_i_send_text(token, '/unknown')
    then_i_get_text(token, 'Oops, I do not understand! Can you repeat your question?')

    app = BotClient.new(token)

    app.run_once
  end

  describe '/offers' do
    it 'should respond no offers available' do
      token = 'fake_token'
      expected_offers = { offers: [] }
      stub_jobvacancy_offers(expected_offers)
      when_i_send_text(token, '/offers')
      then_i_get_text(token, 'There are no job offers available at the moment!')

      app = BotClient.new(token)

      app.run_once
    end

    it 'should respond with current offers' do
      token = 'fake_token'
      expected_offers = { offers: [{ id: 9, title: 'titulo', location: 'location', description: 'description' }] }

      stub_jobvacancy_offers(expected_offers)

      when_i_send_text(token, '/offers')
      then_i_get_text(token, parse_offers(JSON.parse(expected_offers.to_json)))

      app = BotClient.new(token)

      app.run_once
    end
  end

  describe '/register' do
    it 'should respond: User created successfully' do
      token = 'fake_token'
      stub_usuario_creado('usuario1', 'test@test.com', 'password', 'egutter')

      when_i_send_text(token, '/register usuario1, test@test.com, password')
      then_i_get_text(token, 'User created successfully')

      app = BotClient.new(token)

      app.run_once
    end

    it 'should respond with error message when email is invalid' do
      token = 'fake_token'

      stub_usuario_no_creado('usuario1', 'mail_invalido', 'password', "egutter", {'email' => ['invalid email']})

      when_i_send_text(token, '/register usuario1, mail_invalido, password')
      then_i_get_text(token, 'Please enter a valid email')

      app = BotClient.new(token)

      app.run_once
    end

    it 'should respond with error message when email is blank' do
      token = 'fake_token'

      stub_usuario_no_creado('usuario1', '', 'password', 'egutter' , {'email' => ["can't be blank",'invalid email']})

      when_i_send_text(token, '/register usuario1, , password')
      then_i_get_text(token, "The argument <email> cannot be empty!")

      app = BotClient.new(token)

      app.run_once
    end

    it 'should respond with error message when email is blank without , after email' do
      token = 'fake_token'

      stub_usuario_no_creado('usuario1', '', 'password', 'egutter', {'email' => ["can't be blank",'invalid email']})

      when_i_send_text(token, '/register usuario1, ,password')
      then_i_get_text(token, "The argument <email> cannot be empty!")

      app = BotClient.new(token)

      app.run_once
    end

    it 'should respond with error message when email is blank without , after name and after email' do
      token = 'fake_token'

      stub_usuario_no_creado('usuario1', '', 'password', 'egutter', {'email' => ["can't be blank",'invalid email']})

      when_i_send_text(token, '/register usuario1,,password')
      then_i_get_text(token, "The argument <email> cannot be empty!")

      app = BotClient.new(token)

      app.run_once
    end

    it 'should respond with error message when name is blank' do
      token = 'fake_token'

      stub_usuario_no_creado('', 'test@test.com', 'password', 'egutter', {'name' => ["can't be blank"]})

      when_i_send_text(token, '/register , test@test.com, password')
      then_i_get_text(token, "The argument <name> cannot be empty!")

      app = BotClient.new(token)

      app.run_once
    end

    it 'should respond with error message when password is blank' do
      token = 'fake_token'

      stub_usuario_no_creado('name', 'test@test.com', '', 'egutter', {'password' => ["can't be blank"]})

      when_i_send_text(token, '/register name, test@test.com,')
      then_i_get_text(token, "The argument <password> cannot be empty!")

      app = BotClient.new(token)

      app.run_once
    end

    it 'should respond with correct use guidelines when there are missing parameters' do
      token = 'fake_token'
      when_i_send_text(token, '/register usuario1, password')
      then_i_get_text(token, 'Usage: /register <name>, <email>, <password>')
      app = BotClient.new(token)
      app.run_once
    end

    it 'should respond with correct use guidelines when there all parameters are missing' do
      token = 'fake_token'
      when_i_send_text(token, '/register')
      then_i_get_text(token, 'Usage: /register <name>, <email>, <password>')
      app = BotClient.new(token)
      app.run_once
    end
  end

  describe '/apply' do
    it 'should respond Application sent' do
      token = 'fake_token'

      stub_apply('73', 'testmail@dc.uba.ar', 'https://mycv.com', 'Contact information sent.')

      when_i_send_text(token, '/apply 73, testmail@dc.uba.ar, https://mycv.com')
      then_i_get_text(token, 'Application sent!')

      app = BotClient.new(token)

      app.run_once
    end

    it 'should respond The offer you selected does not exist' do
      token = 'fake_token'

      stub_apply('99', 'testmail@dc.uba.ar', 'https://mycv.com', 'That offer does not exist.')

      when_i_send_text(token, '/apply 99, testmail@dc.uba.ar, https://mycv.com')
      then_i_get_text(token, 'The offer you selected does not exist!')

      app = BotClient.new(token)

      app.run_once
    end

    it 'should respond The email you entered is not valid' do
      token = 'fake_token'

      stub_apply('73', 'testuba.ar', 'https://mycv.com', 'Invalid email or empty CV url.')

      when_i_send_text(token, '/apply 73, testuba.ar, https://mycv.com')
      then_i_get_text(token, 'The email you entered is not valid!')

      app = BotClient.new(token)

      app.run_once
    end

    it 'should respond The cv url you entered is not valid' do
      token = 'fake_token'

      stub_apply('73', 'test@uba.ar', 'url no valida', 'Invalid CV url.')

      when_i_send_text(token, '/apply 73, test@uba.ar, url no valida')
      then_i_get_text(token, 'The cv url you entered is not valid!')

      app = BotClient.new(token)

      app.run_once
    end
  end

  describe '/help' do
    it 'should respond with list of commands' do
    token = 'fake_token'
    
    res = print_commands_for("Emilio")

    when_i_send_text(token, '/help')
    then_i_get_text(token, res)

    app = BotClient.new(token)

    app.run_once
    end
  end

  describe '/today_offers' do
    it 'should respond with nice message when no offers today' do
      token = 'fake_token'
      expected_offers = { offers: [] }
      fake_unix_time = 1_557_782_998
      stub_jobvacancy_today_offers(expected_offers, Time.at(fake_unix_time).to_date.to_s)
      when_i_send_text(token, '/today_offers', fake_unix_time)
      then_i_get_text(token, "I'm sorry, it seems that there are no offers posted today!")

      app = BotClient.new(token)

      app.run_once
    end

    it 'should respond with offers of today' do
      token = 'fake_token'
      expected_offers = { offers: [{ id: 9, title: 'titulo', location: 'location', description: 'description' }] }
      fake_unix_time = 1_557_782_998
      stub_jobvacancy_today_offers(expected_offers, Time.at(fake_unix_time).to_date.to_s)
      when_i_send_text(token, '/today_offers', fake_unix_time)
      then_i_get_text(token, parse_offers(JSON.parse(expected_offers.to_json)))

      app = BotClient.new(token)

      app.run_once
    end

    it 'handles JobvacancyConnectionError correctly' do
      token = 'fake_token'
      connector_mock = instance_double(ApiConnector)
      allow(ApiConnector).to receive(:new).and_return(connector_mock)
      allow(connector_mock).to receive(:offers_on_date_service).and_raise(JobvacancyConnectionError)

      fake_unix_time = 1_557_782_998
      stub_jobvacancy_today_offers({}, Time.at(fake_unix_time).to_date.to_s)
      when_i_send_text(token, '/today_offers', fake_unix_time)
      then_i_get_text(token, 'Jobvacancy service is down...')
      
      app = BotClient.new(token)
      app.run_once
    end
  end

  describe '/my_offers' do
    it 'should return user not found message when telegram username is not linked with any user' do
    token = 'fake_token'

    stub_my_offers('User not found', 'egutter')

    when_i_send_text(token, '/my_offers')
    then_i_get_text(token, "I'm sorry, I could not find a user linked with your telegram username")

    app = BotClient.new(token)

    app.run_once
    end

    it 'should return no offers created message when user has not created any' do
      token = 'fake_token'
      
      expected_offers = { offers: [] }
      stub_my_offers(JSON.generate(expected_offers), 'egutter')

      when_i_send_text(token, '/my_offers')
      then_i_get_text(token, 'You have no offers created')

      app = BotClient.new(token)

      app.run_once
    end

    it 'should return offers created by user when user has created offers' do
      token = 'fake_token'
      
      expected_offers = { offers: [{ id: 9, title: 'titulo', location: 'location', description: 'description' }] }
      stub_my_offers(JSON.generate(expected_offers), 'egutter')
  
      when_i_send_text(token, '/my_offers')
      then_i_get_text(token, parse_offers(JSON.parse(expected_offers.to_json)))
  
      app = BotClient.new(token)
  
      app.run_once
    end
  end
end
