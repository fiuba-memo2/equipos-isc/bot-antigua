require 'webmock/rspec'

def stub_jobvacancy_today_offers(expected_offers, date)
  ENV['JOBVACANCY_API_URL'] = 'https://fake_url'
  stub_request(:get, "https://fake_url/api/offers_on?date=#{date}")
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: JSON.generate(expected_offers), headers: {})
end

def stub_jobvacancy_offers(expected_offers)
  ENV['JOBVACANCY_API_URL'] = 'https://fake_url'
  stub_request(:get, "https://fake_url/api/offers")
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: JSON.generate(expected_offers), headers: {})
end


def stub_usuario_creado(usuario, email, password, telegram_username)
  ENV['JOBVACANCY_API_URL'] = 'https://fake_url'
  stub_request(:post, "https://fake_url/api/register").
  with(
    body: {"user"=>{"email"=>email, "name"=>"usuario1", "password"=>password, "telegram_username"=>telegram_username}},
    headers: {
   'Accept'=>'*/*',
   'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
   'User-Agent'=>'Faraday v2.7.4'
    }).
  to_return(status: 200, body: {'success' => true}.to_json, headers: {})
end

def stub_usuario_no_creado(usuario, email, password, telegram_username, errors)
  ENV['JOBVACANCY_API_URL'] = 'https://fake_url'
  stub_request(:post, "https://fake_url/api/register").
  with(
    body: {"user"=>{"email"=>email, "name"=>usuario, "password"=>password, "telegram_username"=>telegram_username}},
    headers: {
   'Accept'=>'*/*',
   'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
   'User-Agent'=>'Faraday v2.7.4'
    }).
  to_return(status: 200, body: {'success' => false, 'errors' => errors}.to_json, headers: {})
end

def stub_apply(offer_id, email, cv_url, message)
  ENV['JOBVACANCY_API_URL'] = 'https://fake_url'
  stub_request(:post, "https://fake_url/api/apply").
  with(
    body: {"offer_id"=>offer_id, "applicant_email"=>email, "url_cv"=>cv_url},
    headers: {
    'Accept'=>'*/*',
    'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
    'Content-Type'=>'application/x-www-form-urlencoded',
    'User-Agent'=>'Faraday v2.7.4'
    }).
  to_return(status: 200, body: message, headers: {})
end

def stub_my_offers(response, telegram_username)
  ENV['JOBVACANCY_API_URL'] = 'https://fake_url'
  stub_request(:get, "https://fake_url/api/my_offers?telegram_username=#{telegram_username}")
    .with(      
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: response, headers: {})
end
